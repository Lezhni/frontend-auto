$(document).ready(function() {
	
	$('.dotdotdot, .car-single').dotdotdot();

	$('.btn-call, .logo-top, .cars-btn').magnificPopup({
		type: 'inline'
	});

    date = "10/27/2014 12:00";
    Timer(date);
    setInterval(function() {
        Timer(date);
    }, 1000);

	$('.form-block1, .form-block7').submit(function(e) {

        var name = $(this).find('.name').val();
        var phone = $(this).find('.phone').val();
        var email = $(this).find('.email').val();
        var code = $(this).find('.code').val();

        if (name == '' || phone == '' || email == '') {
            alert('Заполните поля со свездочкой!');
            return false;
        }

        $.post('sendOrder.php', {
            name: name,
            phone: phone,
            email: email,
            code: code
        },
        function(data) {
            if (data === 'sended') {
                $.magnificPopup.open({
                	items: {
                		src: '#thanks',
                		type: 'inline'
                	}
                });
            } else {
                alert('Сообщение не отправлено');
            }
        });

        return false;

    });

    $('.form-popup-call').submit(function(e) {

        var name = $(this).find('.name').val();
        var phone = $(this).find('.phone').val();
        var email = $(this).find('.email').val();

        if (name == '' || phone == '' || email == '') {
            alert('Заполните поля со свездочкой!');
            return false;
        }

        $.post('send.php', {
            name: name,
            phone: phone,
            email: email
        },
        function(data) {
            if (data === 'sended') {
                $.magnificPopup.open({
                    items: {
                        src: '#thanks',
                        type: 'inline'
                    }
                });
            } else {
                alert('Сообщение не отправлено');
            }
        });

        return false;

    });

    var a = $('.email-top');

    if ($(window).width() < 1400) {
        if (!(a.hasClass('initial'))) {
            $('.email-top').remove();
            a.insertBefore('.phones .phone:first-child').addClass('initial');
        }
    } else {
        if (a.hasClass('initial')) {
            $('.email-top').remove();
            a.insertBefore('.phones').removeClass('initial');
        }
    }
    $(window).resize(function() {

        if ($(window).width() < 1400) {
            if (!(a.hasClass('initial'))) {
                $('.email-top').remove();
                a.insertBefore('.phones .phone:first-child').addClass('initial');
            }
        } else {
            if (a.hasClass('initial')) {
                $('.email-top').remove();
                a.insertBefore('.phones').removeClass('initial');
            }
        }
    });

    /* ScrollSpy */
    
    $('.advantage-single').eq(0).on('scrollSpy:enter', function() {
        var speed = 400;
        if (!$(this).hasClass('showed')) {
            $(this).addClass('showed');
            $(".advantage-single").eq(0).stop().animate({'opacity': 1}, speed);
            $(".advantage-single").eq(4).stop().delay(speed*0.5).animate({'opacity': 1}, speed);
            $(".advantage-single").eq(1).stop().delay(speed).animate({'opacity': 1}, speed);
            $(".advantage-single").eq(5).stop().delay(speed*2).animate({'opacity': 1}, speed);
            $(".advantage-single").eq(2).stop().delay(speed*3).animate({'opacity': 1}, speed);
            $(".advantage-single").eq(6).stop().delay(speed*4).animate({'opacity': 1}, speed);
            $(".advantage-single").eq(3).stop().delay(speed*5).animate({'opacity': 1}, speed);
            $(".advantage-single").eq(7).stop().delay(speed*6).animate({'opacity': 1}, speed);
        }
    });

    $('.numbers').on('scrollSpy:enter', function() {

        if (!$(this).hasClass('showed')) {
            $(this).addClass('showed');
            $({someValue: 0}).animate({someValue: 210}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.clients-number .num').text(Math.round(this.someValue));
                }
            });
            $({someValue1: 0}).animate({someValue1: 10}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.sales-number .num').text(Math.round(this.someValue1));
                }
            });
            $({someValue2: 0}).animate({someValue2: 30}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.cars-number .num').text(Math.round(this.someValue2));
                }
            });
            $({someValue3: 0}).animate({someValue3: 2000}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.parts-number .num').text(Math.round(this.someValue3));
                }
            });
        }
    });

    $('.advantage-single').scrollSpy();
    $('.numbers').scrollSpy();

    $(".cars-list").rcarousel({
        height: 80,
        width: 500,
        visible: 6,
        step: 6,
        margin: 15,
        speed: 3000,
        orientation: 'vertical',
        auto: {enabled: false, direction: "next", interval: 2000}
    });

    $('.cars-more').click(function() {
        $( ".cars-list" ).rcarousel( "next" );

        return false;
    });

});

function Timer(string) {
    var date_new = string;
    var date_t = new Date(date_new);
    var date = new Date();
    var timer = date_t - date;
    if (date_t > date) {
        var day = parseInt(timer / (60 * 60 * 1000 * 24));
        if (day < 10) {
            day = "0" + day;
        }
        day = day.toString();
        var hour = parseInt(timer / (60 * 60 * 1000)) % 24;
        if (hour < 10) {
            hour = "0" + hour;
        }
        hour = hour.toString();
        var min = parseInt(timer / (1000 * 60)) % 60;
        if (min < 10) {
            min = "0" + min;
        }
        min = min.toString();
        var sec = parseInt(timer / 1000) % 60;
        if (sec < 10) {
            sec = "0" + sec;
        }
        sec = sec.toString();
        timethis = day + " : " + hour + " : " + min + " : " + sec;
        $(".days.counter-text").text(day);
        $(".hours.counter-text").text(hour);
        $(".minutes.counter-text").text(min);
        $(".seconds.counter-text").text(sec);
    } else {
         $(".days.counter-text").text("00");
        $(".hours.counter-text").text("00");
        $(".minutes.counter-text").text("00");
        $(".seconds.counter-text").text("00");
    }
}